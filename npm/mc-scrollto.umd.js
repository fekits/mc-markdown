/**
 * 滚动条插件
 *
 * @param o.id      {String}     指定需要滚动到标签位置的ID（与o.to二选其一）
 * @param o.to      {Number}     指定需要滚动的位置（与o.id二选其一）
 * @param o.time    {Number}     指定滚动过程的时间
 * @param o.then    {Function}   滚动到指定位置时回调
 *
 * 版本：v2.0.0
 * */

!(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory();
  } else {
    root.mcScrollTo = factory();
  }
}(this, function () {
  var mcScrollTo = function(o) {
    var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || function(fn) {
      setTimeout(fn, 15);
    };
    var getOffsetTop = function(elements) {
      var top = elements.offsetTop;
      var parent = elements.offsetParent;
      while (parent != null) {
        top += parent.offsetTop;
        parent = parent.offsetParent;
      }
      return top;
    };
    var ease = function(t) {
      return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
    };
    var pos = function(start, end, elapsed, time) {
      return elapsed > time ? end : start + (end - start) * ease(elapsed / time);
    };
    var oIdDom = o.id && document.getElementById(o.id);
    var idEnd = oIdDom && getOffsetTop(oIdDom);
    var time = o.time || 500;
    var clock = Date.now();
    var start = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
    var end = o.to || idEnd || 0;
    (function step() {
      var elapsed = Date.now() - clock;
      window.scrollTo(0, pos(start, end, elapsed, time));
      if (elapsed <= time) {
        requestAnimationFrame(step);
      } else {
        o.then && o.then(end);
      }
    })();
  };
  return mcScrollTo;
}));



