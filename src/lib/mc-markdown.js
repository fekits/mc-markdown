/**
 * v1.0.0
 * */

let mcMarkdown = function (code) {
  // console.log(code);

  let preCode = [];
  code = code.replace(/(`+)([^\n]*)([^`]*)(`+)/g, (S0, S1, S2, S3) => {
    // console.log('S0->', S0);
    // console.log('S1->', S1);
    // console.log('S2->', S2);
    // console.log('S3->', S3);

    // 箭头括号
    S3 = S3.replace(/</g, '&lt;').replace(/>/g, '&gt;');

    // 隔离转存
    preCode.push(`<pre lang="${S2}">${S3}</pre>`);
    return `{__MARK_PRE_${preCode.length}__}`;
  });

  let hCode = [];
  code = code.replace(/(#{1,6})\s+([^\n]*)/g, (S0, S1, S2) => {
    // console.log('S1->', S1, S1.length);
    // console.log('S2->', S2);
    hCode.push(`<h${S1.length} id="${S2}">${S2}</h${S1.length}>`);
    // console.log(hCode);
    return `{__MARK_H_${hCode.length}__}`;
  });

  let aCode = [];
  code = code.replace(/(\[)([^\n[]*)(])(\()([^\n[]*)(\))/g, (S0, S1, S2, S3, S4, S5, S6) => {
    console.log('S0->', S0);
    console.log('S1->', S1);
    console.log('S2->', S2);
    console.log('S3->', S3);
    console.log('S4->', S4);
    console.log('S5->', S5);
    console.log('S6->', S6);
    aCode.push(`<a href="${S5}">${S2}</a>`);
    return `{__MARK_A_${aCode.length}__}`;
  });

  // <a>还原
  aCode.forEach((item, index) => {
    let reg = new RegExp(`{__MARK_A_${index + 1}__}`, 'g');
    code = code.replace(reg, item);
  });

  // <pre>还原
  preCode.forEach((item, index) => {
    let reg = new RegExp(`{__MARK_PRE_${index + 1}__}`, 'g');
    code = code.replace(reg, item);
  });

  // <h1-h6>还原
  hCode.forEach((item, index) => {
    let reg = new RegExp(`{__MARK_H_${index + 1}__}`, 'g');
    code = code.replace(reg, item);
  });

  return code;
};

export default mcMarkdown;
