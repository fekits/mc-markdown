import mcScrollTo from 'mc-scrollto';
import McUrl from 'mc-url';
// 新建一个实例
let urlHash = new McUrl({
  type: '#',
  filter: false
});

// 导航与路由处理
(function () {
  // 获取导航DOM元素
  let aNav = document.getElementById('head-nav-list').children;

  // 滚动触发路由状态,这个状态用于在点击导航，滚动
  let scrollRouteStatus = 1;

  // 导航对象(储存了导航DOM和位置等信息)
  let oNav = [], aNavLength = aNav.length;
  for (let i = 0; i < aNavLength; i++) {
    let routeName = aNav[i].getAttribute('data-route');
    let routeDom = document.getElementById(routeName);
    if (routeDom) {
      oNav.push({ dom: routeDom, top: routeDom.offsetTop, routeName: routeName });
    }

  }
  let toNav = function (routeName) {
    // 设置导航状态
    for (let i = 0; i < aNav.length; i++) {
      if (aNav[i].getAttribute('data-route') === routeName) {
        aNav[i].setAttribute('data-active', 'true');
        urlHash.set({
          key: 'route',
          val: routeName
        });
      } else {
        aNav[i].removeAttribute('data-active');
      }
    }

    // 设置焦点图动画状态，如果当前滚动到焦点图不可见的地方时，关闭焦点图动画效果节省性能资源
    let oBanner = document.getElementById('banner');
    if (routeName !== 'home') {
      // console.log(routeName);
      oBanner.removeAttribute('data-am');
    } else {
      oBanner.setAttribute('data-am', 'true');
    }
  };

  // 路由处理
  let toRoute = function (routeName = 'home') {
    // 设置当前活动的导航
    toNav(routeName);
    // 滚动到导航所在锚点
    mcScrollTo({
      id: routeName,
      time: 600,
      then () {
        setTimeout(function () {
          scrollRouteStatus = 1;
        }, 50);
      }
    });
  };

  // 页面加载时触发路由
  window.addEventListener('load', function () {
    // 如果网址中带有路由参数则处理路由
    let routeName = urlHash.get('route');
    if (routeName) {
      scrollRouteStatus = 0;
      toRoute(routeName);
    }
  });

  // 点击导航时触发路由
  document.addEventListener('click', function (ev) {
    let el = ev.target || ev.srcElement;
    let routeName = el.getAttribute('data-route');
    if (routeName) {
      scrollRouteStatus = 0;
      toRoute(routeName);
      document.getElementById('mc-nav-switch').checked = false;
    }
  });

  // 滚动页面时触发路由
  window.addEventListener('scroll', function () {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
    if (scrollRouteStatus) {
      (function () {
        scrollRouteStatus = 0;
        requestAnimationFrame(function () {
          let oNavLength = oNav.length;
          let lastNav = 'home';
          for (let i = 0; i < oNavLength; i++) {
            if (scrollTop >= oNav[i].top) {
              lastNav = oNav[i].routeName;
            }
          }
          toNav(lastNav);
          scrollRouteStatus = 1;
        });

      })();
    }
  });
})();
