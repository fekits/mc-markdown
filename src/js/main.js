import '../css/main.scss';

import './route';
import mcMarkdown from '../lib/mc-markdown';

// 代码着色插件
import McTinting from 'mc-tinting';
// 新建一个代码着色实例
new McTinting();

import mcAjax from 'mc-ajax';

// 实例代码
mcAjax({
  url: '../data/README.md',
  type: 'GET',
  dataType: 'text',
  success (res) {
    let newCode = mcMarkdown(res);
    console.log(newCode);
    document.getElementById('readme').innerHTML = newCode;
  }
});
