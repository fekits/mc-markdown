#MC-URL

一款URL网址传参操作插件，可以单个或批量的对网址hash或search参数进行增删改查操作。亮点是容错能力强，可批量操作，可以开启错误或无效参数自动过滤。


## 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)


## 演示
[http://www.junbo.name/plugins/mc-url](http://www.junbo.name/plugins/mc-url)


## 开始

下载项目: 

```
npm i mc-url

```

## 参数
```
  // 新建实例的参数
  let myUrl = new McURL({
    type  : '#' | '?',       {String}  必填，填写'#'则新建一个操作hash值的实例，填写'?'则新建一个操作search值的实例
    filter: true | false,    {Boolean} 选填，为true则开启无效参数纠错与过滤
  })
  
  // 操作网址时的参数
  myUrl.set(
    opt,                     {*} 可以是字符，也可以是数组也可以是对象
    urlString                {String} 指定网址，对当前页面的网址进行操作时不要填写此项
  )
       
```

## 示例

```javascript
import McURL from './mc-url';

// 新建一个hash值实例
let hash = new McURL({
    type : '#'
});

// 设置一条
hash.set({
  key: 'aaa',
  val: 111
});

// 设置多条
hash.set([
  {
    key: 'bbb',
    val: 222
  },
  {
    key: 'ccc',
    val: 333
  }
]);

// 删除一条
hash.del('aaa');

// 删除多条
hash.del(['bbb','ccc']);

// 删除全部
hash.del('*');

// 获取一条
hash.get('aaa');

// 获取多条并以数组格式返回
hash.get(['bbb','ccc']);

// 获取多条并以对象格式返回
hash.get({
  a:'aaa',
  bbb:'bbb'
});

// 获取全部
hash.all();

// 是否有某一条？
hash.has('ddd');


// 操作search
let myUrl = new McURL({
  type: '?',
  filter: true
});

// 增，删，改，查跟上面hash一模一样，省略...

// 操作不是当前页面的网址
let oldUrl = 'https://junbo.name?aaa=111&bbb=222&ccc=333';
// 获取一个网址中的一个参数属性值
let aaa = myUrl.get('aaa',oldUrl);
console.log(aaa) // -> 111

let newUrl = myUrl.set([
  {
    key:'ddd',
    val:444
  },
  {
    key:'eee',
    val:555
  }
],oldUrl);

console.log(newUrl) // -> https://junbo.name?aaa=111&bbb=222&ccc=333&ddd=444&eee=555

```

## 版本
```
v2.0.1 [Latest version]
1. 使用UMD方式。
```

```
v2.0.0
1. 重构成构造函数，可以new多个实例并设置为不同的主题同时使用。
2. 优化了新提示冲掉上一条提示时的性能与稳定性问题并让冲掉过程动画更平滑
3. 新增了一款主题mc-toast-ac
4. 文字内容入参名称msg改为text
5. 修复了一个当icon未设置时仍然生成了icon图标占位的小BUG
```

```
v1.0.3
1. 废除回调函数then的用法并新增了on事件群。on里面有show及hide回调
```

```
v1.0.2 
1. 此版本除了模块化使用本插件，还将方法挂载在了window下的fekit对象下，除了模块化引用还可以直接用fekit.mcToast方法。
```
```
v1.0.1
1. 此版本精简了代码，取消了onshow和onhide，回调合并为一个then，可以通赤then回调的传参来判断是显示还是消失及其它一些操作。
```
